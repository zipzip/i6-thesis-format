default:
	pdflatex thesis
	bibtex thesis
	pdflatex thesis
	pdflatex thesis

clean:
	rm -f thesis.log thesis.aux thesis.bbl thesis.blg */*.aux
