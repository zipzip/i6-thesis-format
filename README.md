# README #

A baseline repo for RWTH i6 phd thesis format. Hopefully with this format thesis will be accepted by Ney.

### What is this repository for? ###

* Specify format of latex style
* Additional guidelines for format mentioned in the README

### How do I get set up? ###

* on ubuntu with too many latex packages, I still needed those
* tlmgr install stmaryrd
* tlmgr install breakcites
* make

### Guidelines ###

* Whatever small things that Ney want in the thesis should be added to the baseline.
- Figures: caption below figure.
- Tables: caption above table.
- "Universitätsprofessor" should be replaced by "Professor" 
- Draft version should already contain acknowledgement, abstract etc.
- No specific print version (e.g. with inner and outer margins)
- Chapters may start on uneven pages.
- Margin left / right: not as wide as Christoph Schmidt's. Good example: Joern's draft -> symmetric margins
- Make reasonable use of page space – don’t loose excessive amount of space.
- Scientific goals: Motivation, expectation, why beyond state of the art? Why do I want to do it? Why is it important? 
- Scientific achievements: I was able to improve over XX by YY by doing ZZ.
- Decision rule to be expressed as such: given a f_1^2 → e^_1_2(f_1^2)  given f, find an e as a function of f
- Screen shots/Figures should be large enough.
- Pictures should have fixed ratio, be undistorted.
- In figures, the smallest fonts should be comparable to the fonts in the text.
- Use option "sloppy" (latex).
- British or American English? Not really discussed; Ralf: B.E. is fine.
- Empty line spacing should make the text easy to read – proportions should be reasonable – LaTeX often does the job but not always.
- "Classical" table style is preferred, i.e. vertical lines and bounding lines on all sides.
- No blank space before a comma.
### Who do I talk to? ###

* Someone at i6